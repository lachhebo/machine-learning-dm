import pandas as pd
import numpy as np

from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

import matplotlib.pyplot as plt
import seaborn as sns


def readTrain():
    df = pd.read_csv("data/train.csv")

    return df

def readTest():
    tf = pd.read_csv("data/test.csv")
    return tf

def preprocessData(df): 

    #Delete unknoww entries
    df = df[df != "?"]
    df = df.dropna(axis=0, how='any')
    
    #print((df["workclass"] == "?").sum())
    #print((df['parents'] == "usual").sum() + (df['parents']=="great_pret").sum() + (df['parents']=="pretentious").sum() )
    #print(df.isnull)

    le1 = preprocessing.LabelEncoder()
    le1.fit(["usual","great_pret","pretentious"])
    df[["parents"]] = pd.DataFrame(data =le1.transform(df[["parents"]]))

   # X1 = le1.transform(df[["parents"]])
    #print(type(X1))
    #print(type(df[["parents"]]))

    #assert 0

    le2 = preprocessing.LabelEncoder()
    le2.fit(["less_proper","very_crit","critical","proper","improper"])
    df[["has_nurs"]] = le2.transform(df[["has_nurs"]])
    
    le3 = preprocessing.LabelEncoder()
    le3.fit(["foster","completed","complete","incomplete"])
    df[["form"]] = le3.transform(df[["form"]])

    le4 = preprocessing.LabelEncoder()
    le4.fit(["1","2","3","more"])
    df[["children"]] = le4.transform(df[["children"]])

    le5 = preprocessing.LabelEncoder()
    le5.fit(["critical","less_conv","convenient"])
    df[["housing"]] = le5.transform(df[["housing"]])

    le6 = preprocessing.LabelEncoder()
    le6.fit(["inconv","convenient"])
    df[["finance"]] = le6.transform(df[["finance"]])

    le7 = preprocessing.LabelEncoder()
    le7.fit(["problematic","slightly_prob","nonprob"])
    df[["social"]] = le7.transform(df[["social"]])

    le8 = preprocessing.LabelEncoder()
    le8.fit(["not_recom","recommended","priority"])
    df[["health"]] = le8.transform(df[["health"]])
    
    
    
    #print(df)
    return df
    

def visualiaseData(df):

    sns.pairplot(df)
    plt.show()


def decisiontree():
    ## Read The Data
    df = readTrain()  # test : print(df.head())

    df = preprocessData(df)

    le9 = preprocessing.LabelEncoder()
    le9.fit(["not_recom","spec_prior","very_recom","priority"])

    print(le9.classes_)
    df[["dec"]] = le9.transform(df[["dec"]])

    print( df[["dec"]])
    

    #visualiaseData(df)
    
    ## Divide The Data
    X = df.drop('dec', axis=1)
    y = df["dec"]

    ## Learn & Score

    k_scores = [0]
    for k in range(1,32):
        treek = tree.DecisionTreeClassifier(max_depth=k)
        scores = cross_val_score(treek,X,y,cv = 5, scoring='accuracy')
        k_scores.append(scores.mean())

    
    
    plt.plot(k_scores)
    plt.ylabel('Score')
    plt.xlabel('k')
    
    plt.show()
    


    ## Find the right k :

    treek = tree.DecisionTreeClassifier(max_depth=15)
    treek.fit(X, y)

    ## Test our algorithm on the rest on the test.csv

    
    tf = readTest()
    tf = preprocessData(tf)
    #print(tf)

    result_col2 = treek.predict(tf)

    ## Put the result on a .csv file

    my_csv = pd.DataFrame(data = result_col2, index  = tf.index, columns = ["dec"])
    #print(my_csv)

    my_csv.to_csv(path_or_buf  = "./result/tree_result.csv")
    
    



decisiontree()
