import pandas as pd
import numpy as np
import random as rd
import math

from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

import matplotlib.pyplot as plt
import seaborn as sns


def readTrain():
    df = pd.read_csv("data/train.csv")

    return df

def readTest():
    tf = pd.read_csv("data/test.csv")
    return tf

def preprocessData(df): 

    #Delete unknoww entries
    df = df[df != "?"]
    #df = df.dropna(axis=0, how='any')


    df = df.fillna({'occupation': "Inc"})
    df = df.fillna({'native-country': "Inc"})

    df = df.fillna({'workclass': "A"}, limit=125)
    df = df.fillna({'workclass': "B"}, limit=151)
    df = df.fillna({'workclass': "C"}, limit=1318)
    df = df.fillna({'workclass': "D"}, limit=100)
    df = df.fillna({'workclass': "E"}, limit=150)
    df = df.fillna({'workclass': "F"}, limit=100)


    le = preprocessing.LabelEncoder()
    le.fit(["A","B","C","Z","E","R","T","Y","U","I","O","P","Q","S","D","F","G","H","J","K","L","M","W","X","V","N","AM","AB","AL","AK","AE","AA","AD","AG","AF","AN","AC","AH","AZ","AR","AT","AY","AU","AI","AO","AP","AQ","AS","AJ","AG","AL","AW","AX","AV","AB","Inc"])

    df[["workclass"]] = le.transform(df[["workclass"]])
    df[["education"]] = le.transform(df[["education"]])
    df[["relationship"]] = le.transform(df[["relationship"]])
    df[["race"]] = le.transform(df[["race"]])
    df[["marital-status"]] = le.transform(df[["marital-status"]])
    df[["sex"]] = le.transform(df[["sex"]])
    df[["occupation"]] = le.transform(df[["occupation"]])
    df[["native-country"]] = le.transform(df[["native-country"]])

    return df


def visualiaseData(df):

    sns.pairplot(df)
    plt.show()


def decisiontree():
    ## Read The Data
    df = readTrain()  # test : print(df.head())

    df = preprocessData(df)

    #visualiaseData(df)
    
    ## Divide The Data
    X = df.drop('dec', axis=1)
    y = df["dec"]

    ## Learn & Score

    k_scores = [0]
    for k in range(1,32):
        treek = tree.DecisionTreeClassifier(max_depth=k)
        scores = cross_val_score(treek,X,y,cv = 5, scoring='accuracy')
        k_scores.append(scores.mean())

    
    
    plt.plot(k_scores)
    plt.ylabel('Score')
    plt.xlabel('k')
    
    plt.show()
    
    ## Find the right k :

    treek = tree.DecisionTreeClassifier()
    treek.fit(X, y)

    ## Test our algorithm on the rest on the test.csv

    
    tf = readTest()
    tf = preprocessData(tf)

    result_col2 = treek.predict(tf)

    ## Put the result on a .csv file

    my_csv = pd.DataFrame(data = result_col2, index  = tf.index, columns = ["dec"])
    #print(my_csv)

    my_csv.to_csv(path_or_buf  = "./result/tree_result.csv")

    

decisiontree()

