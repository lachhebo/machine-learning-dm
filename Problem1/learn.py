import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

import matplotlib.pyplot as plt


def readTrain():
    df = pd.read_csv("data/train.csv")
    return df

def readTest():
    tf = pd.read_csv("data/test.csv")
    return tf

def knn():
    ## Read The Data
    df = readTrain()  # test : print(df.head())

    ## Divide The Data
    X = df[["x1","x2","x3","x4","x5","x6","x7","x8","x9","x10","x11","x12","x13","x14","x15","x16"]]
    y = df["dec"]

    ## Learn & Score

    k_scores = [0]
    for k in range(1,32):
        knn = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(knn,X,y,cv = 5, scoring='accuracy')
        k_scores.append(scores.mean())

    
    
    plt.plot(k_scores)
    plt.ylabel('Score')
    plt.xlabel('k')
    
    plt.show()
    
    ## Find the right k :

    knn = KNeighborsClassifier(n_neighbors=1)
    knn.fit(X, y)

    ## Test our algorithm on the rest on the test.csv

    
    tf = readTest()

    print(tf)

    result_col2 = knn.predict(tf)

    ## Put the result on a .csv file

    my_csv = pd.DataFrame(data = result_col2, index  = tf.index, columns = ["dec"])
    #print(my_csv)

    my_csv.to_csv(path_or_buf  = "./result/knn_result.csv")





knn()


